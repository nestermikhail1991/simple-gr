var canvas;
var context;

window.onload = function() {
  canvas = document.getElementById("drawingCanvas");
  context = canvas.getContext("2d");
   
  // Подключаем события
  canvas.onmousedown = startDrawing;
  canvas.onmouseup = stopDrawing;
  canvas.onmouseout = stopDrawing;
  canvas.onmousemove = draw;
}

var previousColorElement;

function changeColor(color, imgElement)
{
    context.strokeStyle = color;
	imgElement.className = "Selected";
	
	if (previousColorElement != null)
	   previousColorElement.className = "";
	   
	previousColorElement = imgElement;
}

var previousThicknessElement;

function changeThickness (thickness, imgElement)
{
    context.lineWidth = thickness;
	imgElement.className = "Selected";
	
	if (previousThicknessElement != null)
	   previousThicknessElement.className = "";
	   
	previousThicknessElement = imgElement;
}

var isDrawing = false;

function startDrawing(e) {
	
	isDrawing = true;
	context.beginPath();
	context.lineCap = 'round';
	
	context.moveTo(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop);
}

function draw(e) {
	if (isDrawing == true)
	{
	  	var x = e.pageX - canvas.offsetLeft;
		var y = e.pageY - canvas.offsetTop;
		
		context.lineTo(x, y);
		context.stroke();
	}
}

function stopDrawing() {
    isDrawing = false;	
}

function clearCanvas() {
	context.clearRect(0, 0, canvas.width, canvas.height);
}

function saveImage() {
	var dataURL = canvas.toDataURL();
	localStorage.setItem('image', dataURL);
}

function loadImage() {
	var dataURL = localStorage.getItem('image');
	var image = new Image();
	image.src = dataURL;
	context.drawImage(image, 0, 0);
}
